﻿var adminApp = angular.module('adminApp');

adminApp.controller('StaticPageController', function ($scope) {

    $scope.File = null;
    $scope.fileSaved = false;
    
    var page = '';
    var filesStore = {};

    $scope.loadAboutFiles = function () {

        if (page == 'About') {
            return;
        }

        page = 'About';

        if (filesStore.About == undefined) {
            $.get('/Admin/ListOfAboutFiles', {}, function (response) {
                $scope.Files = [];
                $scope.NavbarFiles = [];

                for (var i = 0; i < response.length; i++) {
                    var file = { Title: response[i], Content: null };
                    $scope.Files.push(file);
                    $scope.NavbarFiles.push(file);
                }

                filesStore.About = $scope.Files;

                setTimeout(function () {
                    $scope.$apply();
                }, 1);

            });
        }
        else {
            $scope.Files = filesStore.About;
            $scope.NavbarFiles = [];

            for (var i = 0; i < filesStore.About.length; i++) {
                $scope.NavbarFiles.push(filesStore.About[i]);
            }

        }
    };

    $scope.loadPoliceFiles = function () {

        if (page == 'PrivacyPolice') {
            return;
        }

        page = 'PrivacyPolice';
        $scope.File = null;

        if (filesStore.Police == undefined) {
            $.get('/Admin/ListOfPoliceFiles', {}, function (response) {
                $scope.Files = [];
                $scope.NavbarFiles = [];

                for (var i = 0; i < response.length; i++) {
                    var file = { Title: response[i], Content: null };
                    $scope.Files.push(file);
                    $scope.NavbarFiles.push(file);
                }

                filesStore.Police = $scope.Files;

                setTimeout(function () {
                    $scope.$apply();
                }, 1);

            });
        }
        else {
            $scope.Files = filesStore.Police;
            $scope.NavbarFiles = [];

            for (var i = 0; i < filesStore.Police.length; i++) {
                $scope.NavbarFiles.push(filesStore.Police[i]);
            }

        }
    };

    $scope.loadAboutFiles();

    $scope.removeFromNavbar = function (file) {

        window.event.preventDefault();

        var files = [];

        for (var i = 0; i < $scope.NavbarFiles.length; i++) {
            if ($scope.NavbarFiles[i].Title != file.Title) {
                files.push($scope.NavbarFiles[i]);
            }
        }

        $scope.NavbarFiles = files;

        if ($scope.File.Title == file.Title) {
            $scope.File = null;
        }

        setTimeout(function () {
            $scope.$apply();
        }, 1);
    };

    $scope.openFile = function (file) {

        window.event.preventDefault();

        $.get('/Admin/GetFileContent', { page: page, title: file.Title }, function success(data) {
            file.Content = data;
            $scope.File = file;

            setTimeout(function () {
                $scope.$apply();
            }, 1);
        });
    };

    $scope.saveFile = function () {
        //{ page: 'About', title: $scope.File.Title, content: $scope.File.Content }
        $.post("/Admin/SaveFileContent", { page: page, title: encodeURIComponent($scope.File.Title), content: encodeURIComponent($scope.File.Content) },
            function success() {
            $scope.fileSaved = true;

            setTimeout(function () {
                $scope.$apply();
            }, 1);

            setTimeout(function () {
                $scope.fileSaved = false;

                setTimeout(function () {
                    $scope.$apply();
                }, 1);
            }, 1000);
        });
    };

    $scope.moveFileToNavbar = function () {

        window.event.preventDefault();

        var file = null;

        for (var i = 0; i < $scope.Files.length; i++) {
            if ($scope.Files[i].Title == $scope.SearchTitle) {
                file = $scope.Files[i];
                break;
            }
        }

        if (file == null) {
            return;
        }

        var found = false;

        for (var i = 0; i < $scope.NavbarFiles.length; i++) {
            if ($scope.NavbarFiles[i].Title == file.Title) {
                found = true;
                break;
            }
        }

        if (!found) {
            $scope.NavbarFiles.push(file);
        }

        $scope.openFile(file);
    };

});