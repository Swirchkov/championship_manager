﻿var adminApp = angular.module('adminApp');

adminApp.controller('main', function ($scope, $route) {
    setTimeout(function () {
        if ($route.current.$$route.originalPath == '/users') {
            $scope.mode = "Users";
        }
        else if ($route.current.$$route.originalPath == '/static') {
            $scope.mode = 'Static';
        }
        else {
            $scope.mode = 'Tournaments';
        }
    }, 1);
});