﻿var adminApp = angular.module('adminApp');

adminApp.controller('UsersController', function ($scope, selectService) {

    $scope.User = null;
    $scope.isDeleting = false;

    $.get("/Admin/GetFirstUsers", "", function success(response) {
        $scope.Users = response;

        setTimeout(function () {
            $scope.$apply();
        }, 1);
    });

    $scope.searchByPattern = function () {

        window.event.preventDefault();

        $.get('/Admin/FilterByPattern?query=' + $scope.Query, "", function (response) {
            $scope.Users = response;

            setTimeout(function () {
                $scope.$apply();
            }, 1);
        });
    }

    $scope.editUser = function (user) {
        selectService.setUser(user);
    }

    $scope.deleteUser = function (user) {
        window.event.preventDefault();
        $scope.User = user;
        $scope.isDeleting = true;
    };

    $scope.confirmUserDelete = function () {

        window.event.preventDefault();

        $.post("/Admin/DeleteUser", { Id: $scope.User.Id }, function succcess(response) {

            if (response.Message != undefined) {
                $scope.hasDeleteError = true;
                return;
            }

            var users = [];

            for (var i = 0; i < $scope.Users.length; i++) {
                if ($scope.Users[i].Id != $scope.User.Id) {
                    users.push($scope.Users[i]);
                }
            }

            $scope.Users = users;

            setTimeout(function () {
                $scope.$apply();
            }, 1);
        });
    };

    $scope.cancelUserDelete = function () {
        window.event.preventDefault();
        $scope.User = null;
        $scope.isDeleting = false;
    };

});