﻿var adminApp = angular.module('adminApp', ['ngRoute']).config(
    function ($routeProvider) {

        var lang = window.Lang;
        var prefix = "/Scripts/AdminPage/Views/" + lang + "/";

        $routeProvider.when("/tournaments", {
            templateUrl: prefix + "Tournament.html",
            controller: "TournamentController"
        });

        $routeProvider.when('/createTournament', {
            templateUrl: prefix + "CreateTournament.html",
            controller: "CreateTournamentController"
        });

        $routeProvider.when('/manageTournament', {
            templateUrl: prefix + "ManageTournament.html",
            controller: "ManageTournamentController"
        });

        $routeProvider.when('/gamesManagement', {
            templateUrl: prefix + "Games.html",
            controller: "GamesController"
        });

        $routeProvider.when('/users', {
            templateUrl: prefix + "Users.html",
            controller: "UsersController"
        });

        $routeProvider.when('/userManagement', {
            templateUrl: prefix + "ManageUser.html",
            controller: "ManageUserController"
        });

        $routeProvider.when('/static', {
            templateUrl: prefix + "StaticPages.html",
            controller: "StaticPageController"
        });

        $routeProvider.otherwise({ redirectTo: "/tournaments" });
    });

