﻿var adminApp = angular.module("adminApp");

adminApp.service('TournamentFormSender', function ($http, $location) {
    this.uploadForm = function () {
        var fd = new FormData();

        fd.append('Logo', document.getElementsByName("Logo").item(0).files[0]);
        fd.append('Name', document.getElementsByName('Name').item(0).value);
        fd.append('SportKind', document.getElementsByName('SportKind').item(0).value);
        fd.append('ParticipateNumber', document.getElementsByName('ParticipateNumber').item(0).value);

        console.log(fd);

        $http.post('/Admin/CreateTournament', fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data) {
            console.log(data);
            if (data == "Success") {
                console.log("redirecting");
                $location.path("/tournaments");
                return;
            }

            // show message
            $("#CreateTournamentValidation").text(data.Message);
            $("#CreateTournamentValidation").css({ display: "block" });
        });

    }
});