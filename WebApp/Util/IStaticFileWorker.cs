﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Util
{
    internal interface IStaticFileWorker
    {
        IEnumerable<string> GetFileList(string dirName);
        string GetFileContent(string fileName);
        void ReplaceFileContent(string fileName, string content);
    }
}
