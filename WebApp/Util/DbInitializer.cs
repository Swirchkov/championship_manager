﻿using BLL.DTO;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Util
{
    public static class DbInitializer
    {
        public static void Initialize()
        {
            IUserService userService = DependencyResolver.Current.GetService<IUserService>();

            UserDTO user = new UserDTO()
            {
                Name = "admin",
                Surname = "admins",
                Password = "123456",
                Patronymic = "adminer",
                Comment = "admin com",
                Email = "tournamentmanager676@gmail.com",
                PhoneNumber = "099111",
                Photo = "/Files/Images/avatar.png",
                Role = "Admin",
                IsEmailVerified = true
            };

            if (userService.Find(u => u.Email == user.Email).Count() > 0)
            {
                userService.DeleteItem(userService.Find(u => u.Email == user.Email).First().Id);
            }
            
            userService.Register(user);
        }
    }
}