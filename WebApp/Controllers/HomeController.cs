﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Util;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            UserDTO user = (UserDTO)Session["User"];

            if (user != null)
            {
                return RedirectToAction("Index", "Tournament");
            }

            UserDTO restoredUser = CookiesAuthenticationWorker.RestoreFromCookies(Request);

            if (restoredUser != null)
            {
                Session["User"] = restoredUser;
                return RedirectToAction("Index", "Tournament");
            }

            Session["Lang"] = "Ru";

            return RedirectToAction("Index", "About");
        }

        public ActionResult ChangeLang(string lang)
        {
            if (lang != "En" && lang != "Ru")
            {
                return HttpNotFound();
            }

            Session["Lang"] = lang;

            return Json("ok", JsonRequestBehavior.AllowGet);
        }
    }
}